module.exports = function( gulp, plugins ) {
	return function() {
		if ( ! plugins.fileExists( './index.php' ) ) {
			var content = '<?php' + '\n' +
			              '/** ' + '\n' +
			              ' * Front to the WordPress application. This file doesn\'t do anything, but loads' + '\n' +
			              ' * wp-blog-header.php which does and tells WordPress to load the theme.' + '\n' +
			              ' *' + '\n' +
			              ' * @package WordPress' + '\n' +
			              ' */' + '\n' +
			              '' + '\n' +
			              '/**' + '\n' +
			              ' * Tells WordPress to load the WordPress theme and output it.' + '\n' +
			              ' *' + '\n' +
			              ' * @var bool' + '\n' +
			              ' */' + '\n' +
			              'define( \'WP_USE_THEMES\', true );' + '\n' +
			              '' + '\n' +
			              '/** Loads the WordPress Environment and Template */' + '\n' +
			              'require( dirname( __FILE__ ) . \'/wp/wp-blog-header.php\' );';

			return gulp.src( './*' )
			           .pipe( plugins.file( 'index.php', content ) )
			           .pipe( gulp.dest( './' ) );
		}
	};
};
