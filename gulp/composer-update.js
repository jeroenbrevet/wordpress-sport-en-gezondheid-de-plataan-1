module.exports = function( gulp, plugins ) {
	return function() {
		return plugins.composer(
			'update', {
				async: false
			}
		);
	};
};
