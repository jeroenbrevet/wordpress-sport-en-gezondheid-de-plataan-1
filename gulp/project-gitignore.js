module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		var content = '/.idea' + '\n' +
		              '/.htaccess' + '\n' +
		              '/bower_components' + '\n' +
		              '/config.json' + '\n' +
		              '/detail' + '\n' +
		              '/docs' + '\n' +
		              '/plugin-reference.json' + '\n' +
		              '/node_modules' + '\n' +
		              '/npm-debug.log' + '\n' +
		              '/README.html' + '\n' +
		              '/vendor' + '\n' +
		              '' + '\n' +
		              '.sass-cache' + '\n' +
		              '.DS_Store' + '\n' +
		              '*.psd' + '\n' +
		              '[Tt]humbs.db' + '\n' +
		              '.Trashes' + '\n' +
		              '' + '\n' +
		              '/composer' + '\n' +
		              '/composer.phar' + '\n' +
		              '!/composer.json' + '\n' +
		              '' + '\n' +
		              '/wp' + '\n' +
		              '/wp-admin' + '\n' +
		              '/wp-content/cache' + '\n' +
		              '/wp-content/languages' + '\n' +
		              '/wp-content/plugins' + '\n' +
		              '/wp-content/upgrade' + '\n' +
		              '/wp-content/uploads' + '\n' +
		              '/wp-content/w3tc-config' + '\n' +
		              '/wp-content/advanced-cache.php' + '\n' +
		              '/wp-content/db.php' + '\n' +
		              '/wp-content/wp-cache-config.php' + '\n' +
		              '/wp-content/wp-rocket-config' + '\n' +
		              '/wp-includes' + '\n' +
		              '' + '\n' +
		              '/license.txt' + '\n' +
		              '/readme.html' + '\n' +
		              '/wp-activate.php' + '\n' +
		              '/wp-blog-header.php' + '\n' +
		              '/wp-comments-post.php' + '\n' +
		              '/wp-config-local.php' + '\n' +
		              '/wp-config-sample.php' + '\n' +
		              '/wp-cron.php' + '\n' +
		              '/wp-links-opml.php' + '\n' +
		              '/wp-load.php' + '\n' +
		              '/wp-login.php' + '\n' +
		              '/wp-mail.php' + '\n' +
		              '/wp-settings.php' + '\n' +
		              '/wp-signup.php' + '\n' +
		              '/wp-trackback.php' + '\n' +
		              '/xmlrpc.php' + '\n' +
		              '/yarn-error.log' + '\n' +
		              '' + '\n' +
		              '/wp-content/themes/' + config.name + '/.gitignore' + '\n' +
		              '/wp-content/themes/' + config.name + '/_additional' + '\n' +
		              '/wp-content/themes/' + config.name + '/_examples' + '\n' +
		              '/wp-content/themes/' + config.name + '/bower_components' + '\n' +
		              '/wp-content/themes/' + config.name + '/composer.lock' + '\n' +
		              '/wp-content/themes/' + config.name + '/docs' + '\n' +
		              '/wp-content/themes/' + config.name + '/dist' + '\n' +
		              '/wp-content/themes/' + config.name + '/img' + '\n' +
		              '/wp-content/themes/' + config.name + '/README.html' + '\n' +
		              '/wp-content/themes/' + config.name + '/README.md';

		return gulp.src( './*' )
		           .pipe( plugins.file( '.gitignore', content ) )
		           .pipe( gulp.dest( './' ) );
	};
};
