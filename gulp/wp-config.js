module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		if ( ! plugins.fileExists( './wp-config.php' ) ) {
			var content = '<?php' + '\n' +
			              '/**' + '\n' +
			              ' * The base configurations of the WordPress.' + '\n' +
			              ' *' + '\n' +
			              ' * This file has the following configurations: MySQL settings, Table Prefix,' + '\n' +
			              ' * Secret Keys, and ABSPATH. You can find more information by visiting' + '\n' +
			              ' * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}' + '\n' +
			              ' * Codex page. You can get the MySQL settings from your web host.' + '\n' +
			              ' *' + '\n' +
			              ' * This file is used by the wp-config.php creation script during the' + '\n' +
			              ' * installation. You don\'t have to use the web site, you can just copy this file' + '\n' +
			              ' * to \'wp-config.php\' and fill in the values.' + '\n' +
			              ' *' + '\n' +
			              ' * @package WordPress' + '\n' +
			              ' */' + '\n' +
			              '' + '\n' +
			              'if ( file_exists( dirname( __FILE__ ) . \'/wp-config-local.php\' ) ) {' + '\n' +
			              '' + '\n' +
			              '\t' + 'require(dirname( __FILE__ ) . \'/wp-config-local.php\');' + '\n' +
			              '' + '\n' +
			              '} else {' + '\n' +
			              '' + '\n' +
			              '\t' + 'die(\'ERROR: wp-config-local.php is not present!\');' + '\n' +
			              '' + '\n' +
			              '}' + '\n' +
			              '' + '\n' +
			              '/** Create a more logical folder structure. */' + '\n' +
			              'define( \'WP_CONTENT_DIR\', dirname( __FILE__ ) . \'/wp-content\' );' + '\n' +
			              'define( \'WP_CONTENT_URL\', WP_ROOT_URL . \'/wp-content\' );' + '\n' +
			              '' + '\n' +
			              '/** Disallow WordPress file editor. */' + '\n' +
			              'define( \'DISALLOW_FILE_EDIT\', true );' + '\n' +
			              '' + '\n' +
			              '/** Database Table prefix, should be the same across installations. */' + '\n' +
			              'define( \'DB_TABLE_PREFIX\', \'' + config.database[ 'table-prefix' ] + '\' );' + '\n' +
			              '' + '\n' +
			              '/** Database Charset to use in creating database tables. */' + '\n' +
			              'define( \'DB_CHARSET\', \'utf8mb4\' );' + '\n' +
			              '' + '\n' +
			              '/** The Database Collate type. Don\'t change this if in doubt. */' + '\n' +
			              'define( \'DB_COLLATE\', \'\' );' + '\n' +
			              '' + '\n' +
			              '/** Plugin specific constants. */' + '\n' +
			              'define( \'WPCF7_AUTOP\', false );' + '\n' +
			              'define( \'WPCF7_LOAD_CSS\', false );' + '\n' +
			              '' + '\n' +
			              '/**' + '\n' +
			              ' * Authentication Unique Keys and Salts.' + '\n' +
			              ' *' + '\n' +
			              ' * Change these to different unique phrases!' + '\n' +
			              ' * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}' + '\n' +
			              ' * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.' + '\n' +
			              ' *' + '\n' +
			              ' * @since 2.6.0' + '\n' +
			              ' */' + '\n' +
			              'require( dirname( __FILE__ ) . \'/wp-salts.php\' );' + '\n' +
			              '' + '\n' +
			              '/**' + '\n' +
			              ' * WordPress Database Table prefix.' + '\n' +
			              ' *' + '\n' +
			              ' * You can have multiple installations in one database if you give each a unique' + '\n' +
			              ' * prefix. Only numbers, letters, and underscores please!' + '\n' +
			              ' */' + '\n' +
			              '$table_prefix = DB_TABLE_PREFIX;' + '\n' +
			              '' + '\n' +
			              '/**' + '\n' +
			              ' * For developers: WordPress debugging mode.' + '\n' +
			              ' *' + '\n' +
			              ' * Change this to true to enable the display of notices during development.' + '\n' +
			              ' * It is strongly recommended that plugin and theme developers use WP_DEBUG' + '\n' +
			              ' * in their development environments.' + '\n' +
			              ' */' + '\n' +
			              'define( \'WP_DEBUG\', false );' + '\n' +
			              '' + '\n' +
			              '/* That\'s all, stop editing! Happy blogging. */' + '\n' +
			              '' + '\n' +
			              '/** Absolute path to the WordPress directory. */' + '\n' +
			              'if ( ! defined( \'ABSPATH\' ) ) {' + '\n' +
			              '' + '\n' +
			              '\t' + 'define( \'ABSPATH\', dirname( __FILE__ ) . \'/\' );' + '\n' +
			              '' + '\n' +
			              '}' + '\n' +
			              '' + '\n' +
			              '/** Sets up WordPress vars and included files. */' + '\n' +
			              'require_once( ABSPATH. \'wp-settings.php\' );';

			return gulp.src( './*' )
			           .pipe( plugins.file( 'wp-config.php', content ) )
			           .pipe( gulp.dest( './' ) );
		}
	};
};
