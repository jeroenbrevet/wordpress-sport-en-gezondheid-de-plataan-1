module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		if ( ! plugins.fileExists( './wp-config-local.php' ) ) {
			var content = '<?php' + '\n' +
			              '' + '\n' +
			              '// ** URL settings ** //' + '\n' +
			              'define( \'WP_ROOT_URL\', \'' + config.url + '\' );' + '\n' +
			              'define( \'WP_HOME\', \'' + config.url + '\' );' + '\n' +
			              'define( \'WP_SITEURL\', \'' + config.url + '/wp\' );' + '\n' +
			              '' + '\n' +
			              '// ** MySQL settings - You can get this info from your web host ** //' + '\n' +
			              'define( \'DB_NAME\', \'' + config.database.name + '\' );' + '\n' +
			              'define( \'DB_USER\', \'' + config.database.user + '\' );' + '\n' +
			              'define( \'DB_PASSWORD\', \'' + config.database.password + '\' );' + '\n' +
			              'define( \'DB_HOST\', \'' + config.database.hostname + '\' );' + '\n' +
			              '' + '\n' +
			              '// ** Never use cache while developing ** //' + '\n' +
			              'define( \'WP_CACHE\', ' + config.production + ' );';

			return gulp.src( './*' )
			           .pipe( plugins.file( 'wp-config-local.php', content ) )
			           .pipe( gulp.dest( './' ) );
		}
	};
};
