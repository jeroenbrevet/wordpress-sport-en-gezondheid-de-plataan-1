module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		// Which browsers should be prefixed
		var browsers = [
			'ie >= 10',
			'ie_mob >= 10',
			'ff >= 30',
			'chrome >= 34',
			'safari >= 7',
			'opera >= 23',
			'ios >= 7',
			'android >= 4.4',
			'bb >= 10'
		];

		if ( ! config.production ) {
			return gulp.src( paths.src.css + '/*.scss' )
			           .pipe( plugins.sassBulkImport() )
			           .pipe( plugins.sourcemaps.init() )
			           .pipe(
				           plugins.sass(
					           {
						           outputStyle: 'compact',
						           precision: 10
					           }
				           ).on( 'error', plugins.sass.logError )
			           )
			           .pipe( plugins.autoprefixer( browsers ) )
			           .pipe( plugins.size( { title: 'Styles: Theme' } ) )
			           .pipe( plugins.sourcemaps.write() )
			           .pipe( gulp.dest( paths.dist.css ) )
			           .pipe( plugins.browserSync.stream() );
		}

		return gulp.src( paths.src.css + '/*.scss' )
		           .pipe( plugins.sassBulkImport() )
		           .pipe(
			           plugins.sass(
				           {
					           outputStyle: 'compressed',
					           precision: 10
				           }
			           ).on( 'error', plugins.sass.logError )
		           )
		           .pipe( plugins.autoprefixer( browsers ) )
		           .pipe( plugins.size( { title: 'Styles: Theme' } ) )
		           .pipe( plugins.changed( paths.dist.css ) )
		           .pipe( plugins.cssnano( {
			           zindex: false
		           } ) )
		           .pipe( gulp.dest( paths.dist.css ) );
	};
};
