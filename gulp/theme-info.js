function capitalizeFirstLetter( string ) {
	return string.charAt( 0 ).toUpperCase() + string.slice( 1 );
}

module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		return gulp.src( paths.src.root + '/style.css' )
		           .pipe( plugins.replace( /^Theme Name:.*$/m, 'Theme Name: ' + capitalizeFirstLetter( config.name ) ) )
		           .pipe( plugins.replace( /^Author:.*$/m, 'Author: ' + capitalizeFirstLetter( config.name ) ) )
		           .pipe( plugins.replace( /^Bitbucket Theme URI:.*$/m, '' ) )
		           .pipe( plugins.replace( /^\s*\n/gm, '' ) )
		           .pipe( gulp.dest( paths.src.root ) );
	};
};
