module.exports = function( gulp, plugins, paths, config, callback ) {
	return function() {
		if ( ! config.production ) {
			return gulp.src( paths.src.js + '/**/*.js' )
			           .pipe( plugins.sourcemaps.init() )
			           .pipe( plugins.include() )
			           .pipe( plugins.changed( paths.dist.js ) )
			           .pipe( plugins.jshint() )
			           .pipe( plugins.jshint.reporter( 'jshint-stylish' ) )
			           .pipe( plugins.sourcemaps.write() )
			           .pipe( plugins.size( { title: 'Javascripts: Theme' } ) )
			           .pipe( gulp.dest( paths.dist.js ) );
		}

		return gulp.src( paths.src.js + '/**/*.js' )
		           .pipe( plugins.include() )
		           .pipe( plugins.size( { title: 'Javascripts: Theme' } ) )
		           .pipe( plugins.changed( paths.dist.js ) )
		           .pipe( plugins.uglify( { preserveComments: 'some' } ) )
		           .pipe( gulp.dest( paths.dist.js ) );
	};
};
