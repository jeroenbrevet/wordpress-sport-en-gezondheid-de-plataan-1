module.exports = function( gulp, plugins ) {
	return function() {
		return plugins.composer(
			'install', {
				async: false
			}
		);
	};
};
