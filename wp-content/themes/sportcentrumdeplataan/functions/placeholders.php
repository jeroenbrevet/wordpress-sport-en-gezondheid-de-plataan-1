<?php
/**
 * Placeholder implementation
 *
 * The placeholders are used to get page content on an archive page.
 * Page templates for custom post type archives are automaticly created
 * when a custom post type has the `has_archive` property.
 *
 * To fetch the content use something like:
 *
 * <?php
 * if ( ( $post = get_archive( 'post_type' ) ) && ! empty( $post ) ) {
 *     setup_postdata( $post );
 *     the_content();
 *     wp_reset_postdata();
 * } ?>
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.0
 */

if ( ! function_exists( 'get_archive' ) ) {
	/**
	 * Find the placeholder page for a specific archive page.
	 *
	 * @since 3.0
	 *
	 * @param string|bool $post_type The post_type for the placeholder archive
	 *
	 * @return \WP_Post|false If a hit is found return the post object else return false
	 */
	function get_archive( $post_type = false ) {
		// If no post type use the wp_query to find one
		if ( ! $post_type ) {
			global $wp_query;
			$post_type = $wp_query->query_vars['post_type'];
		}

		// Check is post type exist
		if ( post_type_exists( $post_type ) ) {
			$posts = get_posts( [
				'post_type'      => 'page',
				'post_status'    => 'publish',
				'posts_per_page' => 1,
				'orderby'        => 'menu_order',
				'order'          => 'asc',
				'meta_query'     => [
					[
						'key'   => '_wp_page_template',
						'value' => 'archive_' . $post_type,
					],
				],
			] );

			if ( $posts ) {
				return reset( $posts );
			}
		}

		return false;
	}
}

if ( ! function_exists( 'get_404' ) ) {
	/**
	 * Find the 404 placeholder page
	 *
	 * @since 3.0
	 *
	 * @return \WP_Post|false If a hit is found return the post object else return false
	 */
	function get_404() {
		if ( is_404() ) {
			$posts = get_posts( [
				'post_type'      => 'page',
				'post_status'    => 'publish',
				'posts_per_page' => 1,
				'orderby'        => 'menu_order',
				'order'          => 'asc',
				'meta_query'     => [
					[
						'key'   => '_wp_page_template',
						'value' => '404',
					],
				],
			] );

			if ( $posts ) {
				return reset( $posts );
			}
		}

		return false;
	}
}
