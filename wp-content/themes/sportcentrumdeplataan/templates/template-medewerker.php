<?php
/**
 * Template Name: Medewerker Page
 * Template Post Type: post, page
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
get_header();
get_template_part( 'template-parts/content', 'banner' ); ?>
	<div class="article">
		<div class="article__content container">
			<h1><?php the_title(); ?></h1>

			<?php the_content(); ?>
		</div>
	</div>
<?php
get_template_part( 'template-parts/content', 'employee' );
get_template_part( 'template-parts/content', 'latest' );
get_footer();
