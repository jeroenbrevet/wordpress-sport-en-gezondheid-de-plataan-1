<?php
/**
 * Template Name: Full Width Page
 * Template Post Type: post, page
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
get_header(); ?>

<?php the_content(); ?>

<?php get_footer();
