<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the page wrapper div.
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
	<?php wp_head() ?>
</head>

<body <?php body_class() ?>>

	<div class="wrapper">
		<header class="header" data-component="scrolltofixed">
			<div class="header__top">
				<div class="container">
					<div class="row d-flex align-items-center justify-content-between">
						<?php
						$address = get_field( 'address', 'option' );
						$phone   = get_field( 'phone', 'option' );
						$email   = get_field( 'email', 'option' );

						if ( ! empty( $address ) || ! empty( $phone ) || ! empty( $email ) ):?>
							<div class="col-md-9 col-xl-8">
								<ul>
									<?php if ( ! empty( $address ) ): ?>
										<li class="d-none d-lg-inline d-xl-inline"><?php echo $address; ?></li>
									<?php endif;

									if ( ! empty( $phone ) ): ?>
										<li>
											<i class="fa fa-phone" aria-hidden="true"></i>
											<a href="tel:<?php echo strip_phone_number( $phone ); ?>"><?php echo $phone; ?></a>
										</li>
									<?php endif;

									if ( ! empty( $email ) ): ?>
										<li>
											<i class="fa fa-envelope-o" aria-hidden="true"></i>
											<a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						<?php endif; ?>

						<div class="col-md-3 col-xl-4">
							<div class="header__tool">
								<?php get_search_form(); ?>

								<a class="is-toggle" href="#nav">
									<i class="fa fa-bars" aria-hidden="true"></i>

									<?php _t('menu'); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="header__main">
				<div class="container">
					<div class="row d-flex align-items-center justify-content-between">
						<div class="col-sm-4 col-md-3 col-lg-4 col-xl-5">
							<a class="header__logo" href="<?php echo esc_url( home_url() ); ?>">
								<?php if ( ( $logo = get_field( 'header_logo', 'option' ) ) && ! empty( $logo ) ): ?>
									<img src="<?php echo $logo['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
								<?php else: ?>
									<img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>">
								<?php endif; ?>
							</a>
						</div>

						<div class="col-sm-8 col-md-7 col-lg-6 col-xl-5 text-sm-right">
							<?php if ( ( $schedule = get_field( 'schedule_button', 'option' ) ) && ( ! empty( $schedule ) ) ): ?>
								<a href="<?php echo $schedule['url']; ?>" class="btn btn-default">
									<?php echo $schedule['title']; ?>
								</a>
							<?php endif;

							if ( ( $subscription = get_field( 'subscriptions_button', 'option' ) ) && ( ! empty( $subscription ) ) ): ?>
								<a href="<?php echo $subscription['url']; ?>" class="btn btn-primary">
									<?php echo $subscription['title']; ?>
								</a>
							<?php endif; ?>
						</div>

						<div class="col-auto">
							<a class="is-toggle" href="#">
								<i class="fa fa-bars" aria-hidden="true"></i>

								<?php _t('menu'); ?>
							</a>
						</div>

						<div class="header__menu">
							<?php wp_nav_menu( [
								'theme_location' => 'header_menu',
								'container'      => '',
							] ); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="header__shape">
				<img class="is-desktop" src="<?php echo get_template_directory_uri(); ?>/img/header-shape.png" alt="" />

				<img class="is-phone" src="<?php echo get_template_directory_uri(); ?>/img/header-shape-m.png" alt="" />
			</div>

			<?php wp_nav_menu( [
				'theme_location'    => 'header_menu',
				'container'         => 'nav',
				'container_id'      => 'nav',
				'container_class'   => 'header__nav',
			] ); ?>

            <div class="header__tag"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>"><?php _t('Bekijk hier alle nieuwsberichten'); ?></a></div>
		</header>
