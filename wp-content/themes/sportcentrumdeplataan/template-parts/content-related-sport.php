<?php
/**
 * The template part for displaying related sport
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$related = wp_get_post_terms( $post->ID, 'sport_cat', [ "fields" => "ids" ] );

if ( ! empty( $related ) && is_array( $related ) ):

	$args = [
		'post_type'      => 'sport',
		'posts_per_page' => 3,
		'post_not__in'   => $related,
		'tax_query'      => [

			[
				'taxonomy' => 'sport_cat',
				'field'    => 'term_id',
				'terms'    => $related,
			]
		]
	];

	$sport = new WP_Query( $args );

	if ( $sport->have_posts() ): ?>
		<div class="sports sports--related container">
			<h2><?php _t( 'Meer activiteiten binnen deze categorie' ); ?></h2>

			<?php if ( $sport->have_posts() ): ?>
				<div class="sports__list">
					<ul class="row">
						<?php while ( $sport->have_posts() ) {
							$sport->the_post();
							get_template_part( 'template-parts/loop', 'sport' );
						}
						wp_reset_postdata(); ?>
					</ul>
				</div>
			<?php endif;

			if ( ( $archive = get_archive( 'sport' ) ) && ! empty( $archive ) ): ?>
				<a href="<?php echo get_permalink( $archive->ID ); ?>" class="button"><?php _t( 'Meer Bekijken' ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif;
endif; ?>

