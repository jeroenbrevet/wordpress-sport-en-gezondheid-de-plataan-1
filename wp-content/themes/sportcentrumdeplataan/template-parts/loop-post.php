<?php
/**
 * The template used for displaying loop post
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

if ( has_post_thumbnail() ):?>
	<figure>
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'news' ); ?>
		</a>
	</figure>
<?php endif; ?>

<div class="is-wrap">
	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

	<?php the_excerpt(); ?>
</div>