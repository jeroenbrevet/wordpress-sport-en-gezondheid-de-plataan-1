<?php
/**
 * The template part for displaying service content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
if ( have_rows( 'services' ) ):?>
	<div class="service container">
		<ul class="row text-center d-flex justify-content-center">
			<?php while ( have_rows( 'services' ) ): the_row(); ?>
				<li class="col-sm-6 col-md-4">
					<?php if ( ( $image = get_sub_field( 'service_image' ) ) && ! empty( $image ) ) { ?>
						<figure>
							<?php echo wp_get_attachment_image( $image, 'service' ); ?>
						</figure>
					<?php }

					if ( ( $title = get_sub_field( 'service_title' ) ) && ! empty( $title ) ):?>
						<h3><?php echo $title; ?></h3>
					<?php endif;

					the_sub_field( 'service_content' );

					if ( ( $more = get_sub_field( 'page' ) ) && ! empty( $more ) ): ?>
						<a href="<?php echo $more; ?>"><?php _t( 'Lees meer' ); ?></a>
					<?php endif; ?>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
<?php endif; ?>