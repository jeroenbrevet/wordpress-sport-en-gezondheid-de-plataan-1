<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>

<div class="content">
	<p><?php _t( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.' ); ?></p>
</div>