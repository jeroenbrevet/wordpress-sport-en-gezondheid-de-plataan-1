<?php
/**
 * The template used for displaying pagination
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$paginate = paginate_links( [
	'prev_text' => __( '«' ),
	'next_text' => __( '»' ),
] );

if ( ! empty( $paginate ) ):?>
	<div class="paginate container">
		<?php echo $paginate; ?>
	</div>
<?php endif; ?>

