<?php
/**
 * The template used for displaying latest posts
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$news = get_field( 'select_news', false, false );

$args = [
	'post_type'   => 'post',
	'post__in'    => $news,
	'post_status' => 'publish',
	'orderby'     => 'post__in'
];

$related = new WP_Query( $args );

if ( $related->have_posts() && ! empty( $news ) ): ?>
	<div class="latest latest--related">
		<div class="container">
			<h2><?php _t( 'Meer weten over ' );
				the_title(); ?>?</h2>

			<ul class="row">
				<?php while ( $related->have_posts() ): $related->the_post(); ?>
					<li class="col-sm-6 latest__block">
						<?php get_template_part( 'template-parts/loop', 'post' ); ?>
					</li>
				<?php endwhile;
				wp_reset_postdata(); ?>
			</ul>

			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-white"><?php _t( 'Bekijk alle Nieuwsberichten' ); ?></a>
		</div>
	</div>
<?php endif; ?>
