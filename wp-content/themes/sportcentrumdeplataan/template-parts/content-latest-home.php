<?php
/**
 * The template used for displaying latest posts
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$number_of_news = get_field( 'number_of_news', get_option( 'page_on_front' ) );
$button         = get_field( 'news_button', get_option( 'page_on_front' ) );
$args           = [
	'post_type'      => 'post',
	'posts_per_page' => $number_of_news,
];

$recent = new WP_Query( $args );

if ( $recent->have_posts() ): ?>
	<div class="latest">
		<div class="container">
			<?php if ( ( $news_title = get_field( 'news_title', get_option( 'page_on_front' ) ) ) && ! empty( $news_title ) ): ?>
				<h2><?php echo $news_title; ?></h2>
			<?php endif; ?>

			<div class="row" data-component="news-slider">
				<?php while ( $recent->have_posts() ): $recent->the_post(); ?>
					<div class="col-sm-6 latest__block">
						<?php get_template_part( 'template-parts/loop', 'post' ); ?>
					</div>
				<?php endwhile;
				wp_reset_postdata(); ?>
			</div>

			<?php if ( ! empty( $button ) ): ?>
				<a href="<?php echo $button['url']; ?>" class="btn btn-white"><?php echo $button['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
