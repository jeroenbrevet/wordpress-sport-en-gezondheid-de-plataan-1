<?php
/**
 * The template part for displaying newsletter content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

if ( ( $shortcode = get_field( 'form_shortcode', 'option' ) ) && ! empty( $shortcode ) ):?>
	<div class="newsletter">
		<div class="container">
			<?php echo do_shortcode( $shortcode ); ?>
		</div>
	</div>
<?php endif; ?>
