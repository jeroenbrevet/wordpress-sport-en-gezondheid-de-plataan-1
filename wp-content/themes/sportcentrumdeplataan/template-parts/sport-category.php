<?php
/**
 * The template part for displaying content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$args = [
	'orderby'    => 'id',
	'order'      => 'desc',
	'hide_empty' => false,
];

$terms = get_terms( 'sport_cat', $args );

if ( ! empty( $terms ) ): ?>
	<ul class="sports__filter">
		<?php foreach ( $terms as $term ): ?>
			<li <?php echo ( get_queried_object_id() == $term->term_id ) ? 'class="is-active"' : ''; ?>>
				<a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>