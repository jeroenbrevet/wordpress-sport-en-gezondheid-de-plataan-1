<?php
/**
 * The template part for displaying contact content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$title     = get_field( 'form_title', 'option' );
$content   = get_field( 'form_content', 'option' );
$shortcode = get_field( 'request_form', 'option' );
$image     = get_field( 'form_image', 'option' );

if ( ! empty( $title ) || ! empty( $content ) || ! empty( $image ) || ! empty( $request ) ):?>
	<div class="trial">
		<div class="container">
			<div class="row d-flex align-items-end justify-content-between">
				<?php if ( ! empty( $image ) ): ?>
					<div class="col-sm-5">
						<?php echo wp_get_attachment_image( $image, 'contact' ); ?>
					</div>
				<?php endif;

				if ( ! empty( $title ) || ! empty( $content ) ): ?>
					<div class="col-sm-7 trial__content">
						<?php if ( ! empty( $title ) ): ?>
							<h3><?php echo $title; ?></h3>
						<?php endif;

						echo $content;

						if ( ! empty( $shortcode ) ) {
							echo do_shortcode( $shortcode );
						} ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
