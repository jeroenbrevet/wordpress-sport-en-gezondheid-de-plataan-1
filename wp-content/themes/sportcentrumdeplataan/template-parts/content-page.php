<?php
/**
 * The template used for displaying page content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
?>

<?php the_content(); ?>