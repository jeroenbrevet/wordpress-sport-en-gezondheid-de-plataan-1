<?php
/**
 * The template part for displaying contact content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$title     = get_field( 'contact_title' );
$content   = get_field( 'contact_content' );
$photo     = get_field( 'contact_photo' );
$shortcode = get_field( 'form_shortcode' );

if ( ! empty( $title ) || ! empty( $content ) || ! empty( $photo ) || ! empty( $shortcode ) ):?>
	<div class="trial trial--contact">
		<div class="container">
			<div class="row d-flex align-items-end justify-content-between">
				<?php if ( ! empty( $title ) || ! empty( $content ) ): ?>
					<div class="col-sm-7 trial__content">
						<?php if ( ! empty( $title ) ): ?>
							<h3><?php echo $title; ?></h3>
						<?php endif;

						echo $content;

						if ( ! empty( $shortcode ) ) {
							echo do_shortcode( $shortcode );
						} ?>
					</div>
				<?php endif;

				if ( ! empty( $photo ) ): ?>
					<div class="col-sm-5">
						<?php echo wp_get_attachment_image( $photo, 'contact' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
