<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header();
get_template_part( 'template-parts/content', 'banner' ); ?>
<main role="main">
	<div class="article">
		<div class="container">
			<div class="article__content">
				<?php if ( ( $post = get_404() ) && ! empty( $post ) ) :
					setup_postdata( $post );
					the_content();
					wp_reset_postdata();
				else : ?>
					<h1><?php _t( 'Page not Found' ); ?></h1>
				<?php endif; ?>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>
