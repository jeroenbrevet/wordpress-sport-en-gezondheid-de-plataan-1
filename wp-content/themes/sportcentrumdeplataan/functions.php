<?php
/**
 * Theme functions and definitions
 *
 * Set up the theme and initiates the autoloader containing some helper
 * functions, which are used in the theme as custom template tags.
 * Others are attached to action and filter hooks in WordPress to change
 * core functionality.
 *
 * @link       https://codex.wordpress.org/Theme_Development
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 *
 * @link       https://codex.wordpress.org/Plugin_API
 *
 * More information about the theme classes and functions can be found by generating
 * the documentation using:
 *
 * apigen generate --source="./" --destination="docs" --deprecated --exclude="_additional"
 * --internal --php --template-theme="bootstrap" --title="Custom_Theme" --todo --tree
 *
 * @link       http://www.apigen.org
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

/**
 * Automatic theme setup
 * When you need more namespaces you can define them here
 */
require_once( get_theme_file_path( '/src/lib/Autoload.php' ) );
$theme = new \Custom_Theme\Autoload;
$theme->register();
$theme->add_namespace( 'Custom_Theme', get_template_directory() . '/src/lib' );

/**
 * Theme name declaration
 *
 * @since 1.0
 *
 * @var string THEME_NAME Name of the theme
 */
define( 'THEME_NAME', end( explode( '/', get_template_directory() ) ) );

/**
 * Defines all responsive meta data
 *
 * @since 1.0
 *
 * @var bool RESPONSIVE Whether this theme is responsive or not
 */
define( 'RESPONSIVE', true );

/**
 * Do we need the WP admin bar in the frontend?
 *
 * @since 1.0
 *
 * @var bool DISABLE_WP_BAR Disables the admin bar if needed
 */
define( 'DISABLE_WP_BAR', false );

/**
 * If typekit is needed for font loading define here
 *
 * @since 1.0
 *
 * @var mixed TYPEKIT Will contain the typekit code when typekit is needed else false
 */
define( 'TYPEKIT', false );

/**
 * If Google Maps are needed inside the theme
 * When true, you can use google maps in your components like this:
 * <?php wp_enqueue_footer_script( 'maps' ); ?>
 *
 * @since 1.0
 *
 * @var bool GOOGLE_MAPS Will be true if maps are needed else false
 */
define( 'GOOGLE_MAPS', true );

/**
 * Setup the text domain for translation
 *
 * @since 1.0
 *
 * @var string TEXTDOMAIN Name of the text domain
 */
define( 'TEXTDOMAIN', THEME_NAME );

/**
 * Does this theme make use of trackbacks?
 *
 * @since 3.4.6
 *
 * @var bool DISABLE_TRACKBACKS False if trackbacks should be allowed, true (default) if not
 */
define( 'DISABLE_TRACKBACKS', true );

/**
 * Redirects search results from /?s=query to /search/query/, converts %20 to +
 *
 * @since 3.4.6
 *
 * @var bool NICE_SEARCH False if you don't want nice search URLs, true (default) if you do
 */
define( 'NICE_SEARCH', true );

/**
 * Does the theme uses featured images?
 *
 * @since 1.0
 *
 * @var bool FEATURED_IMAGE True when featured images are needed else false
 */
define( 'FEATURED_IMAGE', true );

/**
 * The stylesheets which are used inside the theme
 *
 * @since 1.0
 *
 * @var array STYLESHEETS {
 * @type string $src          The source of the stylesheet file
 * @type mixed  $dependencies Array with keys of dependencies, defaults to false
 * @type mixed  $version      String holding the version, defaults to false
 * @type string $media        The media type for example 'print', defaults to 'all'
 * @type bool   $enqueue      Whether to enqueue this stylesheet on all pages, defaults to false
 * }
 */
define( 'STYLESHEETS', serialize( [
	'main' => [
		'src' => 'dist/css/main.css',
	],
] ) );

/**
 * The JavaScripts which are used inside the theme
 *
 * @internal Since version 4.0 RequireJS is introduced, so including scripts here won't be needed anymore
 * @see      src/js/app.js
 *
 * @since    1.0
 *
 * @var array STYLESHEETS {
 * @type string $src          The source of the javascript file
 * @type mixed  $dependencies Array with keys of dependencies, defaults to false
 * @type mixed  $version      String holding the version, defaults to false
 * @type bool   $in_footer    Whether this file should be loaded in the footer, defaults to true
 * @type bool   $enqueue      Whether to enqueue this javascript file on all pages, defaults to false
 * }
 */
define( 'JAVASCRIPTS', serialize( [
	'require' => [
		'src'          => 'dist/js/vendor/require.js',
		'dependencies' => [ 'jquery' ]
	],
] ) );

/**
 * The image sizes which are used by images in the theme
 *
 * @since 3.0
 *
 * @var array IMAGE_SIZES {
 * @type int   $width  The width of the image
 * @type int   $height The height of the image
 * @type mixed $crop   If false, images will be scaled, not cropped.
 *                     If true (default), images will be cropped to the specified dimensions using center
 *                     positions. If an array in the form of array( x_crop_position, y_crop_position ):
 *                     x_crop_position accepts ‘left’ ‘center’, or ‘right’, y_crop_position accepts ‘top’,
 *                     ‘center’, or ‘bottom’. Images will be cropped to the specified dimensions within the defined
 *                     crop area.
 * }
 */
define( 'IMAGE_SIZES', serialize( [
	'banner'      => [ 1305, 352, true ],
	'home-banner' => [ 1300, 453, true ],
	'service'     => [ 365, 353 ],
	'profile'     => [ 344, 334 ],
	'news'        => [ 555, 210, true ],
	'request'     => [ 458, 231, true ],
	'footer-bg'   => [ 1303, 424, true ],
	'contact'     => [ 397, 408, true ],
	'employee'    => [ 165, 245, true ],
	'sport'       => [ 360, 193, true ],
] ) );

/**
 * The menu positions which are used inside the theme
 *
 * @since 2.0
 *
 * @var array MENUS {
 * @type string $name The name of the menu position
 * }
 */
define( 'MENUS', serialize( [
	'header_menu' => __t( 'Header - Menu' )
] ) );

/**
 * Defines the sidebars which are used in the theme
 *
 * @since 1.0
 *
 * @var array SIDEBARS {
 * @type string $name          The name of the sidebar
 * @type string $id            The ID of the sidebar, need to be unique
 * @type string $before_widget HTML which will be inserted before the widget
 * @type string $after_widget  HTML which will be inserted after the widget
 * @type string $before_title  HTML which will be inserted before the widget title
 * @type string $after_title   HTML which will be inserted after the widget title
 * }
 */
define( 'SIDEBARS', serialize( [
	[
		'name'          => __t( 'Right' ),
		'id'            => 'sidebar_right',
		'before_widget' => '<section id="%s" class="widget %s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="title">',
		'after_title'   => '</p>',
	],
	[
		'name'          => __t( 'Footer Widgets' ),
		'id'            => 'footer_widgets',
		'before_widget' => '<section id="%s" class="widget %s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="title">',
		'after_title'   => '</p>',
	],
] ) );

/**
 * Contains a list of unused widgets
 *
 * @since 2.0
 *
 * @var array REMOVE_WIDGETS {
 * @type string $name The name of the widget to remove
 * }
 */
define( 'REMOVE_WIDGETS', serialize( [
	'WP_Widget_Pages',
	'WP_Widget_Calendar',
	'WP_Widget_Archives',
	'WP_Widget_Links',
	'WP_Widget_Meta',
	'WP_Widget_Search',
	'WP_Widget_Categories',
	'WP_Widget_Recent_Posts',
	'WP_Widget_Recent_Comments',
	'WP_Widget_RSS',
	'WP_Widget_Tag_Cloud',
] ) );

/**
 * Defines the size of the content container
 *
 * @since 1.0
 *
 * @var int CONTENT_WIDTH The size of the content container
 */
define( 'CONTENT_WIDTH', 500 );

/**
 * Defines the symbol of the excerpt more
 *
 * @since 1.0
 *
 * @var string EXCERPT_MORE Will be placed after an excerpt
 */
define( 'EXCERPT_MORE', '...' );

/**
 * The length in words of the excerpt
 *
 * @since 1.0
 *
 * @var int EXCERPT_LENGTH The length of the excerpt
 */
define( 'EXCERPT_LENGTH', 20 );

/**
 * List of template which should not have an editor
 *
 * @since 2.0
 *
 * @var array HIDE_DEFAULT_EDITOR {
 * @type string $name The slug of the template
 * }
 */
define( 'HIDE_DEFAULT_EDITOR', serialize( [ 'templates/template-no-editor.php' ] ) );

/**
 * By default WordPress hides the editor on the defined blog page,
 * this constant can undo it
 *
 * @since 2.0
 *
 * @var bool EDITOR_ON_BLOG_PAGE True if editor need to be hidden, or false (default) when it should be available
 */
define( 'EDITOR_ON_BLOG_PAGE', false );

/**
 * List of post types which should contain the Visual Composer editor
 *
 * @since 2.0
 *
 * @var array VC_POST_TYPES {
 * @type string $name The slug of the post type
 * }
 */
define( 'VC_POST_TYPES', serialize( [ 'page', 'post' ] ) );

/**
 * List of page templates which should contain the Visual Composer editor
 *
 * @since 2.0
 *
 * @var array VC_PAGE_TEMPLATES {
 * @type string $name The slug of the page template
 * }
 */
define( 'VC_PAGE_TEMPLATES', serialize( [ 'default', 'templates/template-example.php' ] ) );

/**
 * List of components which need content fixing.
 * Sometimes WordPress adds empty paragraphs inside a component,
 * define them here and they will be removed.
 *
 * @since 2.0
 *
 * @var array VC_SHORTCODE_FIXES {
 * @type string $name The slug of the shortcode (or Visual Composer component)
 * }
 */
define( 'VC_SHORTCODE_FIXES', serialize( [ 'vc_shortcode_slug' ] ) );

/**
 * Here we can quickly add row classes to the Visual Composer row component.
 *
 * @since 2.0
 *
 * @var array VC_ROW_CLASSES {
 * @type string $name The name of the class which need to be applied
 * }
 */
define( 'VC_ROW_CLASSES', serialize( [
	__t( 'None' ) => '',
] ) );

// Theme init
$theme->init();
