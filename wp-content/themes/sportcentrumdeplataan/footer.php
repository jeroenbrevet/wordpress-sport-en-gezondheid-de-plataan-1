<?php
/**
 * The template for displaying the footer
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
$footer_image = get_field( 'footer_image', 'option' );

get_template_part( 'template-parts/content', 'newsletter' ); ?>
<footer class="footer" style="background-image: url(<?php echo $footer_image['sizes']['footer-bg']; ?>);">
	<div class="container">
		<?php if ( is_active_sidebar( 'footer_widgets' ) ): ?>
			<div class="footer__widgets">
				<div class="row">
					<?php dynamic_sidebar( 'footer_widgets' ); ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="footer__copyright">
			<div class="row">
				<div class="col-md-8">
					<p>
						<?php echo date( 'Y' ); ?>

						<i class="fa fa-copyright" aria-hidden="true"></i>

						<?php _t( ' De Plataan - ' ) ?>

						<span><a href="https://www.brandpepper.nl/" target="_blank"><?php _t( 'Website laten maken door ' ); ?></a>
							<?php _t( 'Brandpepper' ); ?>
						</span>
					</p>
				</div>

				<div class="col-md-4">
					<?php
					$facebook = get_field( 'facebook_link', 'option' );
					$twitter  = get_field( 'twitter_link', 'option' );

					if ( ! empty( $facebook ) || ! empty( $twitter ) ): ?>
						<ol>
							<li>
								<?php _t( 'Volg ons op' ); ?>
							</li>
							<?php if ( ! empty( $facebook ) ): ?>
								<li>
									<a href="<?php echo $facebook; ?>" target="_blank">
										<i class="fa fa-facebook-square" aria-hidden="true"></i>
									</a>
								</li>
							<?php endif;

							if ( ! empty( $twitter ) ): ?>
								<li>
									<a href="<?php echo $twitter; ?>" target="_blank">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
							<?php endif; ?>
						</ol>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>

<?php wp_footer(); ?>

</body>

</html>
