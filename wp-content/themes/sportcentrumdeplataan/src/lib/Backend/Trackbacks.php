<?php

namespace Custom_Theme\Backend;

/**
 * Class Trackbacks
 *
 * Handles all trackbacks related hooks
 *
 * @since      3.4.6
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Backend
 */
class Trackbacks {
	/**
	 * Trackbacks constructor
	 *
	 * @since 3.4.6
	 */
	public function __construct() {
		if ( DISABLE_TRACKBACKS ) {
			add_filter( 'xmlrpc_methods', [ $this, 'filter_xmlrpc_method' ], 10, 1 );
			add_filter( 'wp_headers', [ $this, 'filter_headers' ], 10, 1 );
			add_filter( 'rewrite_rules_array', [ $this, 'filter_rewrites' ] );
			add_filter( 'bloginfo_url', [ $this, 'kill_pingback_url' ], 10, 2 );
			add_action( 'xmlrpc_call', [ $this, 'kill_xmlrpc' ] );
		}
	}

	/**
	 * Disable pingback XMLRPC method
	 *
	 * @internal This function uses the `xmlrpc_methods` filter
	 * @link     https://codex.wordpress.org/XML-RPC_Extending
	 *
	 * @since    3.4.6
	 *
	 * @param array $methods Available methods
	 *
	 * @return array Array without the pingback.ping method
	 */
	public function filter_xmlrpc_method( $methods ) {
		unset( $methods['pingback.ping'] );

		return $methods;
	}

	/**
	 * Remove pingback header
	 *
	 * @internal This function uses the `wp_headers` filter
	 * @link     https://developer.wordpress.org/reference/hooks/wp_headers
	 *
	 * @since    3.4.6
	 *
	 * @param array $headers Array containing headers
	 *
	 * @return array Array without pingback headers
	 */
	public function filter_headers( $headers ) {
		if ( isset( $headers['X-Pingback'] ) ) {
			unset( $headers['X-Pingback'] );
		}

		return $headers;
	}

	/**
	 * Kill trackback rewrite rule
	 *
	 * @internal This function uses the `rewrite_rules_array` filter
	 * @link     https://codex.wordpress.org/Plugin_API/Filter_Reference/rewrite_rules_array
	 *
	 * @since    3.4.6
	 *
	 * @param array $rules Array containing rewrite rules
	 *
	 * @return array Array without trackback rewrite rules
	 */
	public function filter_rewrites( $rules ) {
		foreach ( $rules as $rule => $rewrite ) {
			if ( preg_match( '/trackback\/\?\$$/i', $rule ) ) {
				unset( $rules[ $rule ] );
			}
		}

		return $rules;
	}

	/**
	 * Kill bloginfo('pingback_url')
	 *
	 * @internal This function uses the `bloginfo_url` filter
	 * @link     https://developer.wordpress.org/reference/hooks/bloginfo_url
	 *
	 * @since    3.4.6
	 *
	 * @param string $output The output of the requested option
	 * @param string $show   The name of the option to show
	 *
	 * @return string Empty string when pingback_url is requested
	 */
	public function kill_pingback_url( $output, $show ) {
		if ( $show === 'pingback_url' ) {
			$output = '';
		}

		return $output;
	}

	/**
	 * Disable XMLRPC call
	 *
	 * @internal This function uses the `xmlrpc_call` action
	 * @link     https://developer.wordpress.org/reference/hooks/xmlrpc_call
	 *
	 * @since    3.4.6
	 *
	 * @param string $action The XMLRPC action call
	 *
	 * @return void
	 */
	public function kill_xmlrpc( $action ) {
		if ( $action === 'pingback.ping' ) {
			wp_die( 'Pingbacks are not supported', 'Not Allowed!', [ 'response' => 403 ] );
		}
	}
}
