<?php

namespace Custom_Theme\Frontend;

/**
 * Class Images
 *
 * Handles all image related hooks
 *
 * @since      2.0
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Frontend
 */
class Images {
	/**
	 * Images constructor
	 *
	 * @since 3.4.6
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'set_image_sizes' ] );
		add_filter( 'image_resize_dimensions', [ $this, 'image_crop_dimensions' ], 10, 6 );
	}

	/**
	 * Registers the differtent image sizes which are used
	 * all over the template. Makes use of the `IMAGE_SIZES`
	 * constant in the `functions.php`
	 *
	 * @internal This function is hooked on the `init` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/init
	 *
	 * @since    3.0
	 *
	 * @return void
	 */
	public function set_image_sizes() {
		$image_sizes = unserialize( IMAGE_SIZES );

		// Add favicon sizes
		$image_sizes['favicon_16']  = [ 16, 16, true ];
		$image_sizes['favicon_32']  = [ 32, 32, true ];
		$image_sizes['favicon_96']  = [ 96, 96, true ];
		$image_sizes['favicon_192'] = [ 192, 192, true ];

		foreach ( $image_sizes as $name => $preset ) {
			add_image_size( $name, $preset[0], $preset[1], $preset[2] );
		}
	}

	/**
	 * Improved image resize dimensions for cropping an image
	 *
	 * This hook makes upscaling images possible for resize.
	 *
	 * @internal This function is hooked on the `image_resize_dimensions` filter
	 * @link     https://codex.wordpress.org/Plugin_API/Filter_Reference/image_resize_dimensions
	 *
	 * @since    3.4
	 *
	 * @param null $default Variable to be filtered.
	 * @param int  $orig_w  Original image width in pixels.
	 * @param int  $orig_h  Original image height in pixels.
	 * @param int  $new_w   Destination image width in pixels.
	 * @param int  $new_h   Destination image height in pixels.
	 * @param bool $crop    Flag to enable image croping.
	 *
	 * @return array|null Array containing cropped/upscaled with and height
	 */
	public function image_crop_dimensions( $default = null, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
		if ( ! $crop ) {
			// If no cropping required let WordPress handle it
			return null;
		}

		$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );
		$crop_w     = round( $new_w / $size_ratio );
		$crop_h     = round( $new_h / $size_ratio );
		$s_x        = floor( ( $orig_w - $crop_w ) / 2 );
		$s_y        = floor( ( $orig_h - $crop_h ) / 2 );

		return [ 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h ];
	}
}
