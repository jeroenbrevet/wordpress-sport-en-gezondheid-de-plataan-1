<?php

namespace Custom_Theme\Widgets;

/**
 * Class Example
 *
 * This class contains all Example widget related functions
 *
 * @since      2.0
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Widgets
 */
class Contact extends \WP_Widget {
	/**
	 * Example constructor
	 *
	 * @since 2.0
	 */
	public function __construct() {
		parent::__construct( 'contact', __t( 'Contact' ) );
	}

	/**
	 * Update the widget parameters
	 *
	 * @since 2.0
	 *
	 * @param array $new_instance The new widget instance
	 * @param array $old_instance The old widget instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	/**
	 * Widget admin form
	 *
	 * @since 2.0
	 *
	 * @param array $instance The widget instance
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$title_field_name = 'title';

		echo '<input id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="hidden" value="' . get_field( $title_field_name, 'widget_' . $this->id_base . '-' . $this->number ) . '" />';
		echo '<br />';
	}

	/**
	 * Load the widget HTML
	 *
	 * @since 2.0
	 *
	 * @param array $args     The widget args
	 * @param array $instance The widget instance
	 *
	 * @return void
	 */
	public function widget( $args, $instance ) {
		$widget_id = 'widget_' . $args['widget_id'];

		require( locate_template( 'template-parts/widget-' . strtolower( preg_replace( '/\B([A-Z])/', '_$1', explode( '\\', get_class( $this ) )[2] ) ) . '.php' ) );
	}
}

register_widget( '\Custom_Theme\Widgets\Contact' );
