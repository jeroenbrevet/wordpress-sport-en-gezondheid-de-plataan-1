/**
 * jQuery.mmenu
 *
 * Empower your website with a mobile menu that has the true native app menu experience with sliding submenus.
 */

/* jshint ignore:start */
// =require ../../../../../../node_modules/jquery.mmenu/dist/jquery.mmenu.js
/* jshint ignore:end */
