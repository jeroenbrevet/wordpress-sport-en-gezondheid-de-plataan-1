/**
 * jQuery
 *
 * jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document
 * traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API t
 * hat works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has
 * changed the way that millions of people write JavaScript.
 */

// Use jQuery defined by WordPress
define( 'jquery', [], function() {
	return window.jQuery;
} );