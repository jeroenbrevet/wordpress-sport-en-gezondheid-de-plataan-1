//
// Main
//
// Import this file using the following HTML or equivalent:
// <script src="dist/js/maps.min.js" type="text/javascript"></script>
//
// Example of usage
//
// <div class="maps" data-zoom="5">
//     <?php if ( ( $location = get_field( 'location' ) ) && ! empty( $location ) ) : ?>
//         <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-marker-icon="<?php echo get_theme_file_uri( 'img/marker.png' ); ?>">
//             <div class="maps__infobox">
//                 <p>Infobox content</p>
//             </div>
//         </div>
//     <?php endif; ?>
// </div>
//
jQuery( document ).ready( function( $ ) {
	$( '.maps' ).maps( {
		map: {
			disableDefaultUI: true,
			zoomControl: true
		},
		infobox: {
			closeBoxMargin: '-15px -15px 0px 0px',
			closeBoxURL: 'https://www.google.com/intl/en_us/mapfiles/close.gif',
			pixelOffset: new google.maps.Size( - 30, - 70 ),
			alignBottom: true
		}
	} );
} );

(function( $ ) {
	var maps_functions = {
		map: null,
		map_obj: null,
		options: null,
		markers: null,
		zoom_level: null,

		init: function( map_obj, options ) {
			// Variables
			var _this = this;
			_this.map_obj = map_obj;
			_this.options = options !== undefined ? options : {};
			_this.markers = map_obj.find( '.marker' );
			_this.zoom_level = _this.map_obj.attr( 'data-zoom' ) !== undefined ? parseInt( _this.map_obj.attr( 'data-zoom' ) ) : 14;

			// Map options
			var args = {
				zoom: _this.zoom_level,
				center: new google.maps.LatLng( 0, 0 ),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};

			if ( _this.options.map !== undefined ) {
				$.extend( args, _this.options.map );
			}

			// Create map
			_this.map = new google.maps.Map( _this.map_obj[ 0 ], args );

			// Add a markers reference
			_this.map.markers = [];

			// Add markers
			_this.markers.each( function() {
				_this.add_marker( $( this ) );
			} );

			// Center map
			this.center_map();
		},
		add_marker: function( marker_obj ) {
			// Variables
			var _this = this;
			var latlng = new google.maps.LatLng( marker_obj.attr( 'data-lat' ), marker_obj.attr( 'data-lng' ) );
			var cursor = $( '.maps__infobox', marker_obj ).length > 0 ? 'pointer' : 'default';

			// Create marker
			var marker = new google.maps.Marker( {
				position: latlng,
				map: _this.map,
				cursor: cursor,
				icon: marker_obj.attr( 'data-marker-icon' )
			} );

			// Add to array
			_this.map.markers.push( marker );

			// If marker contains HTML, add it to an infobox
			if ( $( '.maps__infobox', marker_obj ).length > 0 ) {
				// Infobox options
				var args = {
					content: $( '.maps__infobox', marker_obj ).html(),
					boxClass: 'maps__infobox'
				};

				if ( _this.options.infobox !== undefined ) {
					$.extend( args, _this.options.infobox );
				}

				// Create infobox
				_this.map.markers[ _this.map.markers.length - 1 ].infobox = new InfoBox( args );

				// Show infobox when marker is clicked
				google.maps.event.addListener( _this.map.markers[ _this.map.markers.length - 1 ], 'click', function() {
					var current_marker = this;

					$.each( _this.map.markers, function( index, marker ) {
						// If marker is not clicked, close marker
						if ( _this.map.markers[ index ] !== current_marker ) {
							_this.map.markers[ index ].infobox.close();
						}
					} );

					this.infobox.open( _this.map, this );
				} );
			}
		},

		center_map: function() {
			// Variables
			var _this = this;
			var bounds = new google.maps.LatLngBounds();

			// Loop through all markers and create bounds
			$.each( _this.map.markers, function( i, marker ) {
				var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
				bounds.extend( latlng );
			} );

			// Only 1 marker?
			if ( _this.map.markers.length === 1 ) {
				// Set center of map
				_this.map.setCenter( bounds.getCenter() );
			} else {
				// Fit to bounds
				_this.map.fitBounds( bounds );
			}
		}
	};

	$.fn.maps = function( options ) {
		if ( this.length > 0 ) {
			return maps_functions.init( this, options );
		}
	};
}( jQuery ));
