/**
 * Config
 *
 * The RequireJS config for more information about making a config read the documentation
 * http://requirejs.org/docs/api.html#config
 */

requirejs.config( {
	baseUrl: variables.base_url,
	paths: {
		app: '../app',
		components: '../components'
	}
} );