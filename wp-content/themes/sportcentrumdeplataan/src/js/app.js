/**
 * App
 *
 * Load the config so we can build our app
 * When the config is loaded we need jQuery to initialize the component loader.
 */

require( [ 'config' ], function() {
	require( [ 'jquery', 'fastclick' ], function( $, fast_click ) {
		// FastClick polyfill to remove click delays on browsers with touch UIs
		fast_click.attach( document.body );

		// Main
		require( [ 'app/main' ] );

		// Component loader
		$( '[data-component]' ).each( function() {
			require( [ 'components/' + $( this ).attr( 'data-component' ) ] );
		} );
	} );
} );