/**
 * News Slider
 *
 * This component requires jQuery and Slick to function.
 * You can require it on the page using <div data-component="news-slider"></div>
 */

define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="news-slider"]' ).each( function() {
			var $this = $( this );

			$this.slick(
				{
					arrows: false,
					slidesToShow: 4,
					infinite: true,
					rows: 0,
					responsive: [
						{
							breakpoint: 576,
							settings: {
								slidesToShow: 1,
								infinite: true,
								dots: true
							}
						}
					]
				}
			);
		} );
	} );
} );