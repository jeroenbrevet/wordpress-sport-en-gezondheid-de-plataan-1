/**
 * Scroll To Fixed
 *
 * This plugin is used to fix elements on the page (top, bottom, anywhere); however.
 * You can require it on the page using <div data-component="scrolltofixed"></div>
 */

define( [ 'jquery', 'scrolltofixed' ], function( $ ) {
	$( function() {
        if ( $(window).width() > 400 ) {
            $('[data-component="scrolltofixed"]').each(function () {
                var $this = $(this);

                $this.scrollToFixed();
            });
        }
	} );
} );