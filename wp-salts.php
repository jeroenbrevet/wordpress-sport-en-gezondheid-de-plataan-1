<?php
/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '+P5ar,H|&I%u+2RG-q9BXa+jR0d;iOu,e`J{.J8SI8&uKG8ZUfcKs{}%@p>S@}zZ');
define('SECURE_AUTH_KEY',  'hoY)|/D-`{lyLj0oHW:fkCg03W0 .&:+8X5MoW@[m-e*|)EB[*WNNk.97.OJ?;`0');
define('LOGGED_IN_KEY',    'Oq~Mk={:Plb+cc3@5>+I{`v-PsC|=58a,AmO<cta-3C!Wu^@Wthrs-_vSFsQUsp4');
define('NONCE_KEY',        '<58R?l{D wu6S;F.xE=j`s1KBR8d^7GBh5#m$<eEe=q+`^Jo:>)owz8E-4EDa:()');
define('AUTH_SALT',        'B$#+yh]PW#nPpTiu@c$Wnk}a=.hh*GFjk~BtvvgP;>%4kWK9VPpC1I^0^S=+OFQ5');
define('SECURE_AUTH_SALT', '{8K=T#>fLgBODliq[AC`Knq3k2|298X!NLlhwSTCn=H!s$]j4kNOA+`m#<=5^SP`');
define('LOGGED_IN_SALT',   '1M2/|d6IB~gSUC&/F1Ee8G+eXbX<fFH<TTy%uX@ <gZQDrpUWxThFZW|])RX_/$8');
define('NONCE_SALT',       '-,S8iUt65i 2:Qi-8]*Hg(kfN.(rfl5B7KWC{mH+`fH-|mZohU%:NVyQJkt60v*V');
